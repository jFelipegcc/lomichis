<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gato_model extends CI_Model {
 

    public function create($entry, $return_id = FALSE)
    {
        try {
            $this->db->insert('gato', $entry);
            return ($return_id) ? $this->db->insert_id() : TRUE;
        }

        catch(Exception $e) {
            return $e->getMessage();
        }
    }

     public function delete_gato_by_id($id)
    {
        $cmd = "DELETE FROM gato where id_gato = ".$id;

        $query = $this->db->query($cmd);
        return (TRUE) ? TRUE : NULL;
    }

    public function get_gatos_by_user($id)
    {
        $cmd = "SELECT * FROM gato INNER JOIN gato_usuario on gato.gato_id=gato_usuario.gato WHERE usuario = ".$id;

        $query = $this->db->query($cmd);
        return ($query->num_rows() >= 1) ? $query->result() : NULL;
    }
 

     public function get_user_by_id($id)
    {
        $cmd = "SELECT * FROM usuario where id_usaurio = ".$id;

        $query = $this->db->query($cmd);
        return ($query->num_rows() == 1) ? $query->row() : NULL;
    }

    public function update_user_by_id($entry, $user_id)
    {
        try {
            $this->db->set($entry);
            $this->db->where('id_usaurio', $user_id);
            $this->db->update('usuario');

            return ($user_id) ? TRUE : NULL;
        }

        catch(Exception $e) {
            return $e->getMessage();
        }
    }

    public function update($id_especialidad, $nombre_especialidad, $descrip_especialidad, $usuario_mod_especialidad){

        if($id_especialidad == "")
            return NULL;

        $instruccion = "UPDATE especialidades SET ";
        $valores = "";

        if($nombre_especialidad != ""){
            if($valores != "")
                $valores .= ", ";
            $valores .= "nombre_especialidad='$nombre_especialidad'"; 
        }

        if($descrip_especialidad != ""){
            if($valores != "")
                $valores .= ", ";
            $valores .= "descrip_especialidad='$descrip_especialidad'"; 
        }

        if($usuario_mod_especialidad != ""){
            if($valores != "")
                $valores .= ", ";
            $valores .= "usuario_mod_especialidad=$usuario_mod_especialidad"; 
        }

            $instruccion = $instruccion.$valores." WHERE id_especialidad=$id_especialidad"; 

            $this->db->query($instruccion);

            return ($this->db->affected_rows() != 1)? FALSE : TRUE;
         
    }

    public function logic_delete($id)
    {
        $cmd = "update especialidades set estatus_especialidad = 8 where id_especialidad = ".$id;

        $query = $this->db->query($cmd);
            return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }
 
    public function get_especialidades_by_user($user_id)
    {
        $cmd = "SELECT * FROM usuarios_especialidades INNER JOIN especialidades ON usuarios_especialidades.id_especialidad=especialidades.id_especialidad where estatus_especialidad = 3 and id_usuario = ".$user_id;

        $query = $this->db->query($cmd);
        return ($query->num_rows() >= 1) ? $query->result() : NULL;
    }



}

/* End of file Auth_model.php */
/* Location: ./application/controllers/Auth_model.php */
