<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gato extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $this->load->model('usuario_model');
        $registros = array();
        $registros = $this->usuario_model->get_usuarios();

        $data = array();
        $data['registros']          = $registros;      
        $data['_APP']['title']      = "Gatos";
        $data['_APP']['header']     = $this->load->view('public/header_f', $data , TRUE);
        $data['_APP']['footer']     = $this->load->view('public/footer_f', $data, TRUE);
        $data['_APP']['fragment']   = $this->load->view('public/gatos_f', $data, TRUE);
        $data['scripts'][]          = 'app/private/modules/gatos';
        

        $this->load->view('public/landing_v', $data, FALSE);
        //die(var_dump_format($data['registros'] ));
    }

    public function add()
    { 
        json_header();
        /*if (!is_null($this->permiso_id)) {*/
            $this->form_validation->set_rules('id_usuario', 'id_usuario', 'trim|numeric|max_length[7]|required');
            $this->form_validation->set_rules('nombre_gato', 'nombre_gato', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('gato_edad', 'gato_edad', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('caracteristicas_gato', 'caracteristicas_gato', 'trim|alpha_dash|max_length[250]|required');
            $this->form_validation->set_rules('gato_raza', 'gato_raza', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('gato_foto', 'gato_foto', 'trim|required');
            $this->form_validation->set_rules('gato_cumpleanos', 'gato_cumpleanos', 'trim|max_length[50]|alpha_dash|required');
  
            if ($this->form_validation->run() &&  $this->input->is_ajax_request()) {
 
                $id_usuario         = $this->input->post('id_usuario');
                $nombre_gato        = $this->input->post('nombre_gato');
                $gato_edad          = $this->input->post('gato_edad'); 
                $caracteristicas_gato    = $this->input->post('caracteristicas_gato');
                $gato_raza          = $this->input->post('gato_raza');
                $gato_foto          = $this->input->post('gato_foto');
                $gato_cumpleanos    = $this->input->post('gato_cumpleanos');
 
                $arr_insert = array(
                    "gato_nombre"           => $nombre_gato,
                    "gato_edad"             => $gato_edad,
                    "gato_caracteristicas"  => $caracteristicas_gato,
                    "gato_raza"             => $gato_raza,      
                    "gato_foto"             => $gato_foto,
                    "gato_cumpleanos"       => $gato_cumpleanos, 

                );

 

                $this->load->model('gato_model');
                $is_data_insert = $this->gato_model->create($arr_insert, true );
                    if ($is_data_insert) {

                        $arr_multi['usuario'] = $id_usuario;
                        $arr_multi['gato'] = $is_data_insert;

                        $this->load->model('usgato_model');
                        $is_data_insert = $this->usgato_model->create($arr_multi, true );

                            if ($is_data_insert) {
                                echo json_encode(
                                    array(
                                        "response_code" => 200,
                                        "response_type" => 'success',
                                        "message"       => "Gato registrado correctamente",
                                    )
                                );
                            }
                            else {
                                 echo json_encode(
                                    array(
                                        "response_code" => 500,
                                        "response_type" => 'error',
                                        "message"       => $is_data_insert,
                                    )
                                );
                            }    

                    }

                    /*Si no podemos editar y el modelo retorna una excepcion*/
                    else {
                         echo json_encode(
                            array(
                                "response_code" => 500,
                                "response_type" => 'error',
                                "message"       => $is_data_insert,
                            )
                        );
                    }


                //end foreach

                }

                /*Si la validación de campos es incorrecta*/
                else {
                    $err = validation_errors();
                    echo json_encode(
                        array(
                            "response_code" => 403,
                            "response_type" => 'error',
                            "message"       => 'Bad Request '.$err,
                            "error"         => $err,
                        )
                    );
                }
           /* }

            Si no tenemos permisos
            else {
                echo json_encode(
                    array(
                        "response_code" => 401,
                        "response_type" => 'warning',
                        "message"       => "Acceso no autorizado",
                    )
                );
                fuchi_wakala($redir = false);
            }*/
        }


    public function show_gatos()
    { 
      //  json_header();
        /*if (!is_null($this->permiso_id)) {*/
            $this->form_validation->set_rules('id_usuario', 'id_usuario', 'trim|numeric|max_length[7]|required');
  
            if ($this->form_validation->run() &&  $this->input->is_ajax_request()) {
 
                $id_usuario         = $this->input->post('id_usuario');
               

                $this->load->model('usgato_model');
                $data['gatos'] = $this->usgato_model->get_gatos_by_user($id_usuario);

                    $html = $this->load->view('public/tabla_gatos_f', $data, true);
                    echo $html;

                }

                /*Si la validación de campos es incorrecta*/
                else {
                    $err = validation_errors();
                    echo json_encode(
                        array(
                            "response_code" => 403,
                            "response_type" => 'error',
                            "message"       => 'Bad Request '.$err,
                            "error"         => $err,
                        )
                    );
                }
           /* }

            Si no tenemos permisos
            else {
                echo json_encode(
                    array(
                        "response_code" => 401,
                        "response_type" => 'warning',
                        "message"       => "Acceso no autorizado",
                    )
                );
                fuchi_wakala($redir = false);
            }*/
        }

    public function read_gato()
    { 
        json_header();
        /*if (!is_null($this->permiso_id)) {*/

            $this->form_validation->set_rules('id_usuario', 'id_usuario', 'trim|numeric|max_length[7]|required'); 
            $this->form_validation->set_rules('id_gato', 'id_gato', 'trim|numeric|max_length[7]|required'); 
 
            if ($this->form_validation->run() &&  $this->input->is_ajax_request()) {
 
                $id_usuario        = $this->input->post('id_usuario'); 
                $id_gato           = $this->input->post('id_gato'); 
  
                $this->load->model('gato_model');
                $is_user = $this->gato_model->get_gato_by_id($id_gato);
                    if ($is_user) {
                        echo json_encode(
                            array(
                                "response_code" => 200,
                                "response_type" => 'success',
                                "message"       => "El usuario esta registrado en el sitema, mostrando datos...",
                                "data"          => $is_user,
                            )
                        );

                    }

                    /*Si no podemos editar y el modelo retorna una excepcion*/
                    else {
                         echo json_encode(
                            array(
                                "response_code" => 500,
                                "response_type" => 'error',
                                "message"       => $is_user,
                            )
                        );
                    }


                //end foreach

                }

                /*Si la validación de campos es incorrecta*/
                else {
                    $err = validation_errors();
                    echo json_encode(
                        array(
                            "response_code" => 403,
                            "response_type" => 'error',
                            "message"       => 'Bad Request '.$err,
                            "error"         => $err,
                        )
                    );
                }
           /* }

            Si no tenemos permisos
            else {
                echo json_encode(
                    array(
                        "response_code" => 401,
                        "response_type" => 'warning',
                        "message"       => "Acceso no autorizado",
                    )
                );
                fuchi_wakala($redir = false);
            }*/
        }

 
    public function edit()
    { 
        json_header();
        /*if (!is_null($this->permiso_id)) {*/
            $this->form_validation->set_rules('id_usuario', 'id_usuario', 'trim|numeric|max_length[7]|required'); 
            $this->form_validation->set_rules('nombre_usuario', 'nombre_usuario', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('apellido_usuario', 'apellido_usuario', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('correo_usuario', 'correo_usuario', 'trim|valid_email|max_length[50]|required');
            $this->form_validation->set_rules('telefono_usuario', 'telefono_usuario', 'trim|numeric|exact_length[10]|required');
            $this->form_validation->set_rules('direccion_usuario', 'direccion_usuario', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('pass_usuario', 'pass_usuario', 'trim|max_length[50]|alpha_dash');
            $this->form_validation->set_rules('repeat_pass', 'repeat_pass', 'trim|max_length[50]|alpha_dash|matches[pass_usuario]');
 
            if ($this->form_validation->run() &&  $this->input->is_ajax_request()) {
 
                $id_usuario        = $this->input->post('id_usuario');
                $nombre_usuario    = $this->input->post('nombre_usuario');
                $apellido_usuario  = $this->input->post('apellido_usuario'); 
                $correo_usuario    = $this->input->post('correo_usuario');
                $telefono_usuario  = $this->input->post('telefono_usuario');
                $direccion_usuario = $this->input->post('direccion_usuario');
                $pass_usuario      = $this->input->post('pass_usuario');
                $repeat_pass       = $this->input->post('repeat_pass');

                $arr_insert = array(
                    "id_usaurio"                => $id_usuario,
                    "nombre_usuario"            => $nombre_usuario,
                    "apellido_usuario"          => $apellido_usuario,
                    "correo_usuario"            => $correo_usuario,
                    "telefono_usuario"          => $telefono_usuario,      
                    "direccion_usuario"         => $direccion_usuario,
                );

                if($pass_usuario != "" && $pass_usuario != null){
                    $arr_insert['pass_usuario'] = $pass_usuario; 
                }

                $this->load->model('gato_model');
                $is_data_update = $this->gato_model->update_user_by_id($arr_insert, $id_usuario );
                    if ($is_data_update) {
                        echo json_encode(
                            array(
                                "response_code" => 200,
                                "response_type" => 'success',
                                "message"       => "Usuario modificado correctamente",
                            )
                        );

                    }

                    /*Si no podemos editar y el modelo retorna una excepcion*/
                    else {
                         echo json_encode(
                            array(
                                "response_code" => 500,
                                "response_type" => 'error',
                                "message"       => $is_data_update,
                            )
                        );
                    }


                //end foreach

                }

                /*Si la validación de campos es incorrecta*/
                else {
                    $err = validation_errors();
                    echo json_encode(
                        array(
                            "response_code" => 403,
                            "response_type" => 'error',
                            "message"       => 'Bad Request '.$err,
                            "error"         => $err,
                        )
                    );
                }
           /* }

            Si no tenemos permisos
            else {
                echo json_encode(
                    array(
                        "response_code" => 401,
                        "response_type" => 'warning',
                        "message"       => "Acceso no autorizado",
                    )
                );
                fuchi_wakala($redir = false);
            }*/
        }

    public function delete()
    { 
        json_header();
        /*if (!is_null($this->permiso_id)) {*/

            $this->form_validation->set_rules('id_gato', 'id_gato', 'trim|numeric|max_length[7]|required'); 
 
            if ($this->form_validation->run() &&  $this->input->is_ajax_request()) {
 
                $id_gato        = $this->input->post('id_gato');
 
 

                $this->load->model('gato_model');
                $is_data_deleted = $this->gato_model->delete_gato_by_id($id_gato);
                    if ($is_data_deleted) {
                        echo json_encode(
                            array(
                                "response_code" => 200,
                                "response_type" => 'success',
                                "message"       => "El usuario fue eliminado",
                            )
                        );

                    }

                    /*Si no podemos editar y el modelo retorna una excepcion*/
                    else {
                         echo json_encode(
                            array(
                                "response_code" => 500,
                                "response_type" => 'error',
                                "message"       => $is_data_deleted,
                            )
                        );
                    }


                //end foreach

                }

                /*Si la validación de campos es incorrecta*/
                else {
                    $err = validation_errors();
                    echo json_encode(
                        array(
                            "response_code" => 403,
                            "response_type" => 'error',
                            "message"       => 'Bad Request '.$err,
                            "error"         => $err,
                        )
                    );
                }
           /* }

            Si no tenemos permisos
            else {
                echo json_encode(
                    array(
                        "response_code" => 401,
                        "response_type" => 'warning',
                        "message"       => "Acceso no autorizado",
                    )
                );
                fuchi_wakala($redir = false);
            }*/
        }
 
}

