<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $singleton=Session_singleton:: get_instancia();
        

        //die(var_dump($singleton));
        $session_singleton = new Session_singleton();
        
        die(var_dump($session_singleton));
        
        $this->load->model('usuario_model');
        $registros = array();
        $registros = $this->usuario_model->get_usuarios();
        
        $data = array();
        $data['registros']          = $registros;      
        $data['_APP']['title'] = "Usuarios";
        $data['_APP']['header'] = $this->load->view('public/header_f', $data , TRUE);
        $data['_APP']['footer'] = $this->load->view('public/footer_f', $data, TRUE);
        $data['_APP']['fragment'] = $this->load->view('public/usuarios_f', $data, TRUE);
        $data['scripts'][]          = 'app/private/modules/test';
        
        


        $this->load->view('public/landing_v', $data, FALSE);
        //die(var_dump_format($data['registros'] ));
    }

    public function add()
    { 
        json_header();
        /*if (!is_null($this->permiso_id)) {*/

            $this->form_validation->set_rules('nombre_usuario', 'nombre_usuario', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('apellido_usuario', 'apellido_usuario', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('correo_usuario', 'correo_usuario', 'trim|valid_email|max_length[50]|required');
            $this->form_validation->set_rules('telefono_usuario', 'telefono_usuario', 'trim|numeric|exact_length[10]|required');
            $this->form_validation->set_rules('direccion_usuario', 'direccion_usuario', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('pass_usuario', 'pass_usuario', 'trim|max_length[50]|alpha_dash|required');
            $this->form_validation->set_rules('repeat_pass', 'repeat_pass', 'trim|max_length[50]|alpha_dash|required');
 
            if ($this->form_validation->run() &&  $this->input->is_ajax_request()) {
 
                //$id_usuario        = $this->input->post('id_usuario');
                $nombre_usuario    = $this->input->post('nombre_usuario');
                $apellido_usuario  = $this->input->post('apellido_usuario'); 
                $correo_usuario    = $this->input->post('correo_usuario');
                $telefono_usuario  = $this->input->post('telefono_usuario');
                $direccion_usuario = $this->input->post('direccion_usuario');
                $pass_usuario      = $this->input->post('pass_usuario');
                $repeat_pass       = $this->input->post('repeat_pass');

                $arr_insert = array(
                    "nombre_usuario"            => $nombre_usuario,
                    "apellido_usuario"          => $apellido_usuario,
                    "correo_usuario"            => $correo_usuario,
                    "telefono_usuario"          => $telefono_usuario,      
                    "direccion_usuario"         => $direccion_usuario,
                    "pass_usuario"              => $pass_usuario, 

                );

                $this->load->model('usuario_model');
                $is_data_insert = $this->usuario_model->create($arr_insert, true );
                    if ($is_data_insert) {
                        echo json_encode(
                            array(
                                "response_code" => 200,
                                "response_type" => 'success',
                                "message"       => "Usuario registrado correctamente",
                            )
                        );

                    }

                    /*Si no podemos editar y el modelo retorna una excepcion*/
                    else {
                         echo json_encode(
                            array(
                                "response_code" => 500,
                                "response_type" => 'error',
                                "message"       => $is_data_insert,
                            )
                        );
                    }


                //end foreach

                }

                /*Si la validación de campos es incorrecta*/
                else {
                    $err = validation_errors();
                    echo json_encode(
                        array(
                            "response_code" => 403,
                            "response_type" => 'error',
                            "message"       => 'Bad Request '.$err,
                            "error"         => $err,
                        )
                    );
                }
           /* }

            Si no tenemos permisos
            else {
                echo json_encode(
                    array(
                        "response_code" => 401,
                        "response_type" => 'warning',
                        "message"       => "Acceso no autorizado",
                    )
                );
                fuchi_wakala($redir = false);
            }*/
        }

    public function read_user()
    { 
        json_header();
        /*if (!is_null($this->permiso_id)) {*/

            $this->form_validation->set_rules('id_usuario', 'id_usuario', 'trim|numeric|max_length[7]|required'); 
 
            if ($this->form_validation->run() &&  $this->input->is_ajax_request()) {
 
                $id_usuario        = $this->input->post('id_usuario'); 
  
                $this->load->model('usuario_model');
                $is_user = $this->usuario_model->get_user_by_id($id_usuario);
                    if ($is_user) {
                        echo json_encode(
                            array(
                                "response_code" => 200,
                                "response_type" => 'success',
                                "message"       => "El usuario esta registrado en el sitema, mostrando datos...",
                                "data"          => $is_user,
                            )
                        );

                    }

                    /*Si no podemos editar y el modelo retorna una excepcion*/
                    else {
                         echo json_encode(
                            array(
                                "response_code" => 500,
                                "response_type" => 'error',
                                "message"       => $is_user,
                            )
                        );
                    }


                //end foreach

                }

                /*Si la validación de campos es incorrecta*/
                else {
                    $err = validation_errors();
                    echo json_encode(
                        array(
                            "response_code" => 403,
                            "response_type" => 'error',
                            "message"       => 'Bad Request '.$err,
                            "error"         => $err,
                        )
                    );
                }
           /* }

            Si no tenemos permisos
            else {
                echo json_encode(
                    array(
                        "response_code" => 401,
                        "response_type" => 'warning',
                        "message"       => "Acceso no autorizado",
                    )
                );
                fuchi_wakala($redir = false);
            }*/
        }

 
    public function edit()
    { 
        json_header();
        /*if (!is_null($this->permiso_id)) {*/
            $this->form_validation->set_rules('id_usuario', 'id_usuario', 'trim|numeric|max_length[7]|required'); 
            $this->form_validation->set_rules('nombre_usuario', 'nombre_usuario', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('apellido_usuario', 'apellido_usuario', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('correo_usuario', 'correo_usuario', 'trim|valid_email|max_length[50]|required');
            $this->form_validation->set_rules('telefono_usuario', 'telefono_usuario', 'trim|numeric|exact_length[10]|required');
            $this->form_validation->set_rules('direccion_usuario', 'direccion_usuario', 'trim|alpha_dash|max_length[50]|required');
            $this->form_validation->set_rules('pass_usuario', 'pass_usuario', 'trim|max_length[50]|alpha_dash');
            $this->form_validation->set_rules('repeat_pass', 'repeat_pass', 'trim|max_length[50]|alpha_dash|matches[pass_usuario]');
 
            if ($this->form_validation->run() &&  $this->input->is_ajax_request()) {
 
                $id_usuario        = $this->input->post('id_usuario');
                $nombre_usuario    = $this->input->post('nombre_usuario');
                $apellido_usuario  = $this->input->post('apellido_usuario'); 
                $correo_usuario    = $this->input->post('correo_usuario');
                $telefono_usuario  = $this->input->post('telefono_usuario');
                $direccion_usuario = $this->input->post('direccion_usuario');
                $pass_usuario      = $this->input->post('pass_usuario');
                $repeat_pass       = $this->input->post('repeat_pass');

                $arr_insert = array(
                    "id_usaurio"                => $id_usuario,
                    "nombre_usuario"            => $nombre_usuario,
                    "apellido_usuario"          => $apellido_usuario,
                    "correo_usuario"            => $correo_usuario,
                    "telefono_usuario"          => $telefono_usuario,      
                    "direccion_usuario"         => $direccion_usuario,
                );

                if($pass_usuario != "" && $pass_usuario != null){
                    $arr_insert['pass_usuario'] = $pass_usuario; 
                }

                $this->load->model('usuario_model');
                $is_data_update = $this->usuario_model->update_user_by_id($arr_insert, $id_usuario );
                    if ($is_data_update) {
                        echo json_encode(
                            array(
                                "response_code" => 200,
                                "response_type" => 'success',
                                "message"       => "Usuario modificado correctamente",
                            )
                        );

                    }

                    /*Si no podemos editar y el modelo retorna una excepcion*/
                    else {
                         echo json_encode(
                            array(
                                "response_code" => 500,
                                "response_type" => 'error',
                                "message"       => $is_data_update,
                            )
                        );
                    }


                //end foreach

                }

                /*Si la validación de campos es incorrecta*/
                else {
                    $err = validation_errors();
                    echo json_encode(
                        array(
                            "response_code" => 403,
                            "response_type" => 'error',
                            "message"       => 'Bad Request '.$err,
                            "error"         => $err,
                        )
                    );
                }
           /* }

            Si no tenemos permisos
            else {
                echo json_encode(
                    array(
                        "response_code" => 401,
                        "response_type" => 'warning',
                        "message"       => "Acceso no autorizado",
                    )
                );
                fuchi_wakala($redir = false);
            }*/
        }

    public function delete()
    { 
        json_header();
        /*if (!is_null($this->permiso_id)) {*/

            $this->form_validation->set_rules('id_usuario', 'id_usuario', 'trim|numeric|max_length[7]|required'); 
 
            if ($this->form_validation->run() &&  $this->input->is_ajax_request()) {
 
                $id_usuario        = $this->input->post('id_usuario');
 
 

                $this->load->model('usuario_model');
                $is_data_deleted = $this->usuario_model->delete_user_by_id($id_usuario);
                    if ($is_data_deleted) {
                        echo json_encode(
                            array(
                                "response_code" => 200,
                                "response_type" => 'success',
                                "message"       => "El usuario fue eliminado",
                            )
                        );

                    }

                    /*Si no podemos editar y el modelo retorna una excepcion*/
                    else {
                         echo json_encode(
                            array(
                                "response_code" => 500,
                                "response_type" => 'error',
                                "message"       => $is_data_deleted,
                            )
                        );
                    }


                //end foreach

                }

                /*Si la validación de campos es incorrecta*/
                else {
                    $err = validation_errors();
                    echo json_encode(
                        array(
                            "response_code" => 403,
                            "response_type" => 'error',
                            "message"       => 'Bad Request '.$err,
                            "error"         => $err,
                        )
                    );
                }
           /* }

            Si no tenemos permisos
            else {
                echo json_encode(
                    array(
                        "response_code" => 401,
                        "response_type" => 'warning',
                        "message"       => "Acceso no autorizado",
                    )
                );
                fuchi_wakala($redir = false);
            }*/
        }
 
}

