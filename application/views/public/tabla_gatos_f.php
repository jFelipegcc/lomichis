 
      <!-- Tables
        ================================================== -->
           <div class="row">
            <div class="col-lg-12">
              <div class="page-header">
                <h1 id="tables">GATOS DE ESTE USUARIO</h1>
              </div>
<!--  -->
 
              <?php if(isset($gatos) && $gatos != null){ ?>

                <?php foreach ($gatos as $gato) { ?>
                  
                  <!-- card -->
                  <div class="card">
                      <div class="card-body">
                        <h4 class="card-title" align='center'><?= $gato->gato_nombre ?></h4>
                        <h6 class="card-subtitle mb-2 text-muted"> <img src="<?= $gato->gato_foto ?>" alt="" class="img-fluid d-block mx-auto" style="max-height: 150px !important;"></h6>
                        <p class="card-text"><?= $gato->gato_caracteristicas ?>, <?= $gato->gato_raza ?>, su cumpleaños es el <?= $gato->gato_cumpleanos ?></p>
                        <a href="#" class="card-link">Card link</a>
                        <a href="#" class="card-link">Another link</a>
                      </div>
                  </div>
                <!-- ./card -->
                  <?php 
                }

                ?>
                <?php
              }else{ ?>

                   <div class="card col-md-12">
                      <div class="card-body">
                        <h4 class="card-title">GATOS DE ESTE USUARIO</h4>
                        <h6 class="card-subtitle mb-2 text-muted">No hay gatos para este usuario</h6>
                      </div>
                  </div>


            <?php
              } ?>
            </div>
          </div>
 