 

<div class="container m-4 p-2">
  <div class="page-header" id="banner">
    <div class="row">

    </div>
  </div>

      <!-- Forms
        ================================================== -->
        <div class="bs-docs-section">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-header">
                <h1 id="forms">Gatos de los usuarios</h1>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">
              <div class="bs-component">

                <div class="card  mt-3 mb-3" >
                 <div class="card-body">
                  <h5 class="card-title">Selecciona un usuario</h5>
                  <form id="form-usuarios"> 
                    <div class="d-flex flex-wrap justify-content-around">

                      <div class="form-group col-md-12 col-sm-12">
                        <label for="exampleSelect1" class="form-label mt-4">ID usuario</label>
                        <select class="form-select" id="exampleSelect1">
                      <?php if (isset($registros) && $registros != null) {
                        foreach ($registros as $row) { ?>
                        <option value="<?php echo($row->id_usaurio); ?>"><?php echo($row->nombre_usuario." ");?> <?php echo($row->apellido_usuario); ?></option> 
                      <?php
                        }
                      }else{ ?>
                      <tr class="table-light">
                        <option value="0">No hay registros</option>
                      </tr> 
                      <?php
                      }
                      ?>
                    </select>


                      </div>
 
                    </div> 
                  </form>     
                </div>
              </div>

<!--  -->

<!-- <?php var_dump_format($registros); ?> -->

      <!-- Tables
        ================================================== -->
        <div class="bs-docs-section mt-4" id="tabla-gatos">
 
        </div>
                <div class="card  mt-3 mb-3" >
                 <div class="card-body">
                  <h5 class="card-title">Registrar gatos para este usuario</h5>
                  <form id="form-gatos"> 
                    <div class="d-flex flex-wrap justify-content-around">

                          <input type="text" class="form-control" id="usuario_id" aria-describedby="emailHelp" placeholder="ID del propietario del gato" > 
 
                      <div class="form-group col-md-5 col-sm-12">
                        <label for="gato_id" class="form-label mt-4">ID gato</label>
                        <input type="text" class="form-control" id="gato_id" aria-describedby="emailHelp" placeholder="ID del gato" readonly> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="nombre_gato" class="form-label mt-4">Nombre del gato</label>
                        <input type="text" class="form-control" id="nombre_gato" aria-describedby="nombre_gato" placeholder="Nombre"> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="gato_edad" class="form-label mt-4">Edad gato</label>
                        <input type="number" class="form-control" id="gato_edad" aria-describedby="gato_edad" placeholder="Edad"> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="caracteristicas_gato" class="form-label mt-4">Caracteristicas</label>
                        <input type="text" class="form-control" id="caracteristicas_gato" aria-describedby="caracteristicas_gato" placeholder="Caracteristicas"> 
                      </div>                                        

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="gato_raza" class="form-label mt-4">Raza</label>
                        <input type="text" class="form-control" id="gato_raza" aria-describedby="gato_raza" placeholder="Raza"> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="gato_foto" class="form-label mt-4">Imagen</label>
                        <input type="text" class="form-control" id="gato_foto" aria-describedby="gato_foto" placeholder="Direccion"> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="gato_cumpleanos" class="form-label mt-4">Cumpleanos</label>
                        <input type="date" class="form-control" id="gato_cumpleanos" aria-describedby="emailHelp" placeholder="Cumpleanos"> 
                      </div> 

                    </div>
                    <div class="d-flex justify-content-around mt-5">
                      <button type="button" class="btn btn-primary btn-save" id="btn-save">Guardar</button>
                      <button type="button" class="btn btn-light btn-clear" id="btn-clear">Limpiar forma</button>
                      <!-- <button type="button" class="btn btn-danger" id="btn-cancel">Cancelar</button> -->
                      

                    </div>
                  </form>     
                </div>
              </div>
      </div>
    </div>
  </div>


</div></div>

<!-- 

array(1) {
  [0]=>
  object(stdClass)#22 (7) {
    ["id_usaurio"]=>
    string(1) "1"
    ["nombre_usuario"]=>
    string(6) "alicia"
    ["apellido_usuario"]=>
    string(2) "le"
    ["correo_usuario"]=>
    string(19) "alicia@lomichis.com"
    ["telefono_usuario"]=>
    string(10) "4422101616"
    ["direccion_usuario"]=>
    string(4) "aaaa"
    ["pass_usuario"]=>
    string(10) "1234567890"
  }
}

 -->