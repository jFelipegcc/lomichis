<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?=app_name()?> | <?=$_APP['title']?></title>
    <link rel="shortcut icon" href="<?=base_url('static/images/favicon.ico')?>" type="image/x-icon">
    <link rel="icon" href="<?=base_url('static/images/favicon.ico')?>" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <link rel="stylesheet" href="<?=base_url('static/admin/font/iconsmind-s/css/iconsminds.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/font/simple-line-icons/css/simple-line-icons.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/css/vendor/bootstrap.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/css/vendor/bootstrap.rtl.only.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/css/vendor/bootstrap-float-label.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/css/main.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/css/dore.light.blue.css')?>" />    
    <link rel="stylesheet" href="<?=base_url('static/admin/fa-5.7.2/css/all.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/toastr/toastr.min.css')?>" />
</head>

<body class="background show-spinner">
    <div class="fixed-background"></div>
    <main>
        <div class="container">
        <div class="row h-100 justify-content-center align-items-center">

        <div class="col-lg-5 col-md-7 mt-2">

             <div class="row h-100">
                <div class="col-12 col-md-10 mx-auto my-auto">
                    <div class="card p-3 mt-2">
                                    <div class="card-header bg-transparent pb-0">
              <div class="text-muted text-center mt-3"><small>Ingresa con tus credenciales </small></div>
            </div>
                        <div class="form-side">                            
                            <?php if ($this->session->flashdata('message')) : ?>
                                <div class="alert alert-<?=$this->session->flashdata('message_type')?> alert-dismissible fade show  mb-3" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="fas fa-times"></i>
                                    </button>
                                    <?=$this->session->flashdata('message')?>
                                </div>
                            <?php endif; ?>

                            <img src="<?=base_url('static/images/leappi-logo.png')?>" class="img-fluid d-block mx-auto mb-4" style="max-height: 100px">
                            

                            <h6 class="mb-4">Iniciar sesión</h6>
                            <form id="form-login" method="post" autocomplete="off" action="<?=base_url('login/auth')?>">
                                <label class="form-group has-float-label mb-4">
                                    <input class="form-control" name="username" id="username" required maxlength="70" placeholder="Ingresa tu teléfono o e-mail registrado para iniciar sesión" />
                                    <span>Correo eléctronico</span>
                                </label>

                                <label class="form-group has-float-label mb-4">
                                    <input class="form-control" name="password" id="password" type="password" placeholder="Ingresa tu contraseña" required maxlength="16" />
                                    <span>Contraseña</span>
                                </label>
                                <div class="">
                                        <div class='d-flex justify-content-end text-muted m-2' style='background-color: transparent;' >
                                            <a href='<?php echo base_url('recuperar'); ?>'> ¿Olvidaste tu contraseña?  </a>
                                        </div>
                                        <div class='d-flex justify-content-end text-muted m-2' style= 'background-color: transparent;'>
                                            <a href='<?php echo base_url('registro'); ?>'> Registrate gratis </a>
                                        </div>
                                 </div>                                
                                <div class="d-flex justify-content-end mt-5 pt-5">
                                    <button id="form-login-btn-sbmt" type="submit" class="btn btn-primary btn-lg btn-shadow default" type="submit">Acceder</button>                                 
                                </div>
                            </form>
                        </div>
                     </div>  
                 </div> 
            </div>
        </div>

</div>
</div>


    </main>

    <script type="text/javascript"> function base_url() { return "<?=base_url()?>" } </script>
    <script src="<?=base_url('static/admin/js/vendor/jquery-3.3.1.min.js')?>"></script>
    <script src="<?=base_url('static/admin/js/vendor/bootstrap.bundle.min.js')?>"></script>    
    <script src="<?=base_url('static/admin/toastr/toastr.min.js')?>"></script>
    <script src="<?=base_url('static/admin/js/dore.script.js')?>"></script>
    <script src="<?=base_url('static/admin/js/scripts.single.theme.js')?>"></script>        
    <script src="<?=base_url('static/admin/js/app/public/login.js')?>"></script>    
</body>

</html>
