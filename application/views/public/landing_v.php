<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>lomichis</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="Saas, Software, multi-uses, HTML, Clean, Modern" />
    <meta name="author" content="Shreethemes" />
    <meta name="email" content="support@shreethemes.in" />
    <meta name="website" content="https://shreethemes.in" />
    <meta name="Version" content="v3.2.0" />
    <!-- favicon -->
    <link rel="shortcut icon" href="<?=base_url('static/images/favicon.ico')?>">
    <!-- Font Awesome -->
    <link href="<?=base_url('static/css/fontawesome/css/all.css')?>" rel="stylesheet">
    <link src="<?=base_url('static/css/fontawesome/css/all.css')?>" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?=base_url('static/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
               
 
</head>

<body>
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div> 
    <!-- Loader -->

    <?=$_APP['header']?>

    <?=$_APP['fragment']?>

    <?=$_APP['footer']?>

    <!-- Back to top -->
    <a href="#" onclick="topFunction()" id="back-to-top" class="btn btn-icon btn-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
    <!-- Back to top -->


     <!-- javascript -->
    <script src="<?=base_url('static/js/bootstrap.bundle.min.js')?>"></script>
 <script type="text/javascript"> function base_url(complement = '') { return "<?= base_url() ?>"+complement } </script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<?php if (isset($scripts)):
    foreach ($scripts as $script): ?>
        <script type="text/javascript" src="<?= base_url(
            'static/js/' . $script . '.js'
        ) ?>"></script>
    <?php endforeach;
endif; ?>

    <script src="<?=base_url('static/js/app.js')?>"></script><!--Note: All important javascript like page loader, menu, sticky menu, menu-toggler, one page menu etc. -->
</body>
</html>
