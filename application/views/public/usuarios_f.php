 

<div class="container m-4 p-2">
  <div class="page-header" id="banner">
    <div class="row">

    </div>
  </div>

      <!-- Forms
        ================================================== -->
        <div class="bs-docs-section">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-header">
                <h1 id="forms">Usuarios del sitio</h1>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">
              <div class="bs-component">


                <div class="card  mt-3 mb-3" >
                 <div class="card-body">
                  <h5 class="card-title">Registrar usuario</h5>
                  <form id="form-usuarios"> 
                    <div class="d-flex flex-wrap justify-content-around">

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="id_usuario" class="form-label mt-4">ID usuario</label>
                        <input type="text" class="form-control" id="id_usuario" aria-describedby="emailHelp" placeholder="ID de usuario" readonly> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="nombre_usuario" class="form-label mt-4">Nombre(s)</label>
                        <input type="text" class="form-control" id="nombre_usuario" aria-describedby="nombre_usuario" placeholder="Nombre (s)"> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="apellido_usuario" class="form-label mt-4">Apellido(s)</label>
                        <input type="text" class="form-control" id="apellido_usuario" aria-describedby="apellido_usuario" placeholder="Apellidos (s)"> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="correo_usuario" class="form-label mt-4">Correo electronico</label>
                        <input type="email" class="form-control" id="correo_usuario" aria-describedby="correo_usuario" placeholder="Correo electronico"> 
                      </div>                                        

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="telefono_usuario" class="form-label mt-4">Telefono</label>
                        <input type="text" class="form-control" id="telefono_usuario" aria-describedby="telefono_usuario" placeholder="Telefono"> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="direccion_usuario" class="form-label mt-4">Direccion</label>
                        <input type="text" class="form-control" id="direccion_usuario" aria-describedby="direccion_usuario" placeholder="Direccion"> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="pass_usuario" class="form-label mt-4">Contraseña</label>
                        <input type="password" class="form-control" id="pass_usuario" aria-describedby="emailHelp" placeholder="Contraseña"> 
                      </div>

                      <div class="form-group col-md-5 col-sm-12">
                        <label for="repeat_pass" class="form-label mt-4">Repita la contraseña</label>
                        <input type="password" class="form-control" id="repeat_pass" aria-describedby="emailHelp" placeholder="Verificar contraseña"> 
                      </div>

                    </div>
                    <div class="d-flex justify-content-around mt-5">
                      <button type="button" class="btn btn-primary btn-save" id="btn-save">Guardar</button>
                      <button type="button" class="btn btn-light btn-clear" id="btn-clear">Limpiar forma</button>
                      <!-- <button type="button" class="btn btn-danger" id="btn-cancel">Cancelar</button> -->
                      

                    </div>
                  </form>     
                </div>
              </div>

<!-- <?php var_dump_format($registros); ?> -->

      <!-- Tables
        ================================================== -->
        <div class="bs-docs-section mt-4">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-header">
                <h1 id="tables">TODOS LOS USUARIOS</h1>
              </div>

              <div class="bs-component">
                <table class="table table-hover">
                  <thead>
                    <tr class="table-dark">
                      <th scope="col"><small>ID</small></th>
                      <th scope="col"><small>Nombre (s)</small></th>
                      <th scope="col"><small>Apellido (s)</small></th>
                      <th scope="col"><small>Correo electronico</small></th>
                      <th scope="col"><small>Telefono</small></th>
                      <th scope="col"><small>Direccion</small></th>
                      <th scope="col" colspan="2"><small>Acciones</small></th>
                    </tr>
                  </thead>
                  <tbody>
 
                    <?php if (isset($registros) && $registros != null) {
                      foreach ($registros as $row) { ?>
                      <tr class="table-light">
                      <th><?php echo($row->id_usaurio); ?></th>
                      <th><?php echo($row->nombre_usuario); ?></th>
                      <th><?php echo($row->apellido_usuario); ?></th>
                      <th><?php echo($row->correo_usuario); ?></th>
                      <th><?php echo($row->telefono_usuario); ?></th>
                      <th><?php echo($row->direccion_usuario); ?></th>
                      <th>
                        <button type="button" class="btn btn-info btn-sm d-inline p-2 btn-userread" data-id="<?php echo $row->id_usaurio; ?>"> <i class="fas fa-edit"></i></button>
                        <button type="button" class="btn btn-danger btn-sm d-inline p-2 btn-delete" data-id="<?php echo $row->id_usaurio; ?>"> <i class="far fa-trash-alt"></i></button>
                        <button type="button" onclick='misgatos(<?php echo $row->id_usaurio ?>)' class="btn btn-success btn-sm d-inline p-2 btn-miscat" data-id="<?php echo $row->id_usaurio; ?>"> <i class="fas fa-eye"></i></button>
                      
                      </th>
                      </tr> 
                    <?php
                      }
                    }else{ ?>
                    <tr class="table-light">
                      <th scope="row">No hay registros</th>
                    </tr> 
                    <?php
                    }
                    ?>


                  </tbody>
                </table>
              </div><!-- /example -->
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>


</div></div>


<div class="modal fade" id="gatos_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mis gatos</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div id='gatos_list'>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- 

array(1) {
  [0]=>
  object(stdClass)#22 (7) {
    ["id_usaurio"]=>
    string(1) "1"
    ["nombre_usuario"]=>
    string(6) "alicia"
    ["apellido_usuario"]=>
    string(2) "le"
    ["correo_usuario"]=>
    string(19) "alicia@lomichis.com"
    ["telefono_usuario"]=>
    string(10) "4422101616"
    ["direccion_usuario"]=>
    string(4) "aaaa"
    ["pass_usuario"]=>
    string(10) "1234567890"
  }
}

 -->