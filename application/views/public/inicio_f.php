<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Landrick - Saas & Software Landing Page Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta
      name="description"
      content="Premium Bootstrap 5 Landing Page Template"
    />
    <meta
      name="keywords"
      content="Saas, Software, multi-uses, HTML, Clean, Modern"
    />
    <meta name="author" content="Shreethemes" />
    <meta name="email" content="support@shreethemes.in" />
    <meta name="website" content="https://shreethemes.in" />
    <meta name="Version" content="v3.2.0" />
    <!-- favicon -->
     <!-- Icons -->
    <link
      href="<?= base_url('static/admin/css/materialdesignicons.min.css') ?>"
      rel="stylesheet"
      type="text/css"
    />
    <link
      rel="stylesheet"
      href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"
    />
    <!-- Slider -->
    <link rel="stylesheet" href="<?= base_url(
        'static/admin/css/tiny-slider.css'
    ) ?>" />
    <!-- Main Css -->
   </head>

  <body>
    <!-- Loader -->
    <!-- <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div> -->
    <!-- Loader -->

    <!-- Navbar STart -->
    <header id="topnav" class="defaultscroll sticky bg-white">
      <div class="container">
        <!-- Logo container-->
        <a class="logo" href="<?= base_url() ?>">
          <img
            src="<?= base_url('static/images/logo-dark.png') ?>"
            height="24"
            class="logo-light-mode"
            alt=""
          />
          <img
            src="<?= base_url('static/images/images/logo-light.png') ?>"
            height="24"
            class="logo-dark-mode"
            alt=""
          />
        </a>
        <div class="buy-button">
          <a
            href="<?= base_url('registro') ?>"
            class="btn btn-primary"
          >
            Registrate
          </a>
        </div>
        <div class="buy-button">
          <a
            href="<?= base_url('login') ?>"
            class="btn btn-primary"
          >
            Entrar
          </a>
        </div>
        
        <!--end login button-->
        <!-- End Logo container-->
        <div class="menu-extras">
          <div class="menu-item">
            <!-- Mobile menu toggle-->
            <a class="navbar-toggle" id="isToggle" onclick="toggleMenu()">
              <div class="lines">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </a>
            <!-- End mobile menu toggle-->
          </div>
        </div>

        <div id="navigation">
          <!-- Navigation Menu-->
<!--           <ul class="navigation-menu">
            <li><a href=" " class="sub-menu-item">Home</a></li>
          </ul> -->
          <!--end navigation menu-->
          <div class="buy-menu-btn d-none">
            <a
              href=""
              target="_blank"
              class="btn btn-primary"
            >
              Buy Now
            </a>
          </div>
          <!--end login button-->
        </div>
        <!--end navigation-->
      </div>
      <!--end container-->
    </header>
    <!--end header-->
    <!-- Navbar End -->

    <!-- Hero Start -->
    <section class="home-slider position-relative">
      <div
        id="carouselExampleInterval"
        class="carousel slide"
        data-bs-ride="carousel"
      >
        <div class="carousel-inner">
          <div class="carousel-item active" data-bs-interval="3000">
            <div
              class="bg-home-75vh d-flex align-items-center"
              style="background: url('<?= base_url() ?>static/images/headers/leappi-header-1.webp') center center;"
            >
              <div class="bg-overlay"></div>
              <div class="container"> 
                <!--end row-->
              </div>
            </div>
            <!--end slide-->
          </div>

          <div class="carousel-item" data-bs-interval="3000">
            <div
              class="bg-home-75vh d-flex align-items-center"
              style="background: url('<?= base_url() ?>static/images/headers/leappi-header-2.webp') center center;"
            >
              <div class="bg-overlay"></div>
              <div class="container">
                <div class="row mt-5 justify-content-center">
                  <div class="col-12">
                    <div class="title-heading text-center">
                      <h2 class="text-white title-dark mb-3">
                        Bienvenido a leappi
                      </h2>
 
                      <p class="para-desc mx-auto text-white-50 mb-0">
                        La solución rápida y sencilla para crear y modificar contratos.
                      </p>
                      <div class="mt-4">
                        <a href="javascript:void(0)" class="btn btn-primary">
                          Ver más
                        </a>
                      </div>
                    </div>
                  </div>
                  <!--end col-->
                </div>
                <!--end row-->
              </div>
            </div>
            <!--end slide-->
          </div>
 
        </div>
        <a
          class="carousel-control-prev"
          href="#carouselExampleInterval"
          role="button"
          data-bs-slide="prev"
        >
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </a>
        <a
          class="carousel-control-next"
          href="#carouselExampleInterval"
          role="button"
          data-bs-slide="next"
        >
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </a>
      </div>
    </section>
    <!--end section-->
    <!-- Hero End -->

    <section class="section">
      <div class="container mt-4 mt-lg-0">
        <div class="row align-items-center mb-4 pb-2">
          <div class="col-md-7">
            <div class="section-title text-center text-md-start">
              <h2 class="mb-4 text-primary">FACILITA LOS TRÁMITES ADMINISTRATIVOS</h2>
              <p class="text-muted mb-0 para-desc">
                Los procedimientos legales y administrativos
                <span class="text-primary fw-bold"> </span>
                nunca volverán a ser dolorosos.
              </p>
            </div>
          </div>
          <!--end col-->

          <div class="col-md-5 mt-4 mt-sm-0">
                <div class="row m-3">
                  <div class="col-lg-4 details order-2 order-lg-1">
                    <img src="<?php echo base_url(
                        'static/images/icons/fast-time.png'
                    ); ?>" alt="" class="img-fluid">
                  </div>
                  <div class="col-lg-8 text-left order-1 order-lg-2">
                    <h3>Simple y rapido</h3>
                     <p>Genere documentos a su medida y comience su proceso en poco tiempo</p>
                  </div>
                </div>


                <div class="row m-3">
                  <div class="col-lg-4 details order-2 order-lg-1">
                    <img src="<?php echo base_url(
                        'static/images/icons/bank.png'
                    ); ?>" alt="" class="img-fluid">
                  </div>
                  <div class="col-lg-8 text-left order-1 order-lg-2">
                    <h3>Economico</h3>
                     <p>Ahorre en sus honorarios legales</p>
                  </div>
                </div> 

                <div class="row m-3">
                  <div class="col-lg-4 details order-2 order-lg-1">
                    <img src="<?php echo base_url(
                        'static/images/icons/deal.png'
                    ); ?>" alt="" class="img-fluid">
                  </div>
                  <div class="col-lg-8 text-left order-1 order-lg-2">
                    <h3>Confiable</h3>
                     <p>Consulte a persona experto en cualquier momento y cuente con documentos adaptados a sus necesidades</p>
                  </div>
                </div>                                 

<!--             <div class="text-center text-md-end">
              <a href="javascript:void(0)" class="btn btn-soft-primary">
                See More
                <i data-feather="arrow-right" class="fea icon-sm"></i>
              </a>
            </div> -->
          </div>
          <!--end col-->
        </div>
        <!--end row-->

        <div class="row">

          <div class="col-lg-12 col-md-12 p-3">
            
            <div class="col-12 p-2">
                <h4 class="text-primary text-center"><span>¿Eres abogado y quieres ofrecer tus servicios?</span><br> Únete a nosotros y genera ingresos útiles para ti</h4>
                <div class="d-flex justify-content-center p-3">
                  <a href="<?php echo base_url(
                      'registro'
                  ); ?>" class="btn btn-primary btn-block col-md-4 col-sm-12" style="border-radius: 0.25rem;font-weight: bold; text-transform: capitalize;">REGISTRATE</a>
                </div>
            </div>
            
 
          </div> 
        </div>
        <!--end row-->

 
        <div class="row">

          <div class="col-lg-12 col-md-12 p-3">
            <div class="col-12 p-2">
                <h4 class="text-primary text-center"><span>¿Cómo funciona?</span></h4>
                <div class="d-flex justify-content-center p-3">
                    <div class="row">
                        <div class="col-md-3 col-sm-12 mb-4">
                            <div class="card dashboard-large-chart">
                                <div class="card-body text-center">
                                  <h6 class="text-primary text-center"><b>Registrate</b><br>Responde preguntas sencillas sobre tu identidad. Todos los usuarios son validados por nuestro equipo.</h6>
                                  <img src="<?php echo base_url(
                                      'static/images/icons/form.png'
                                  ); ?>" width="50px" height="50px" alt="" class="img-fluid">
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 mb-4">
                            <div class="card dashboard-large-chart">
                                <div class="card-body text-center">
                                  <h6 class="text-primary text-center"><b>Organiza</b><br> Elige tu horario  y planifica tu jornada con facilidad </h6>
                                  <img src="<?php echo base_url(
                                      'static/images/icons/calendar.png'
                                  ); ?>" width="50px" height="50px" alt="" class="img-fluid">
                                </div> 

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 mb-4">
                            <div class="card dashboard-large-chart">
                                <div class="card-body text-center">
                                  <h6 class="text-primary text-center"><b>Crea </b><br>Crea tus documentos y publicalos desde la plataforma</h6>
                                  <img src="<?php echo base_url(
                                      'static/images/icons/map.png'
                                  ); ?>" width="50px" height="50px" alt="" class="img-fluid">
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 mb-4">
                            <div class="card dashboard-large-chart">
                                <div class="card-body text-center">
                                  <h6 class="text-primary text-center"><b></b>Brinda tus servicios juridicos a clientes por medio de la plataforma, tu decides que clientes aceptar y que servicios ofrecer</h6>
                                  <img src="<?php echo base_url(
                                      'static/images/icons/value.png'
                                  ); ?>" width="50px" height="50px" alt="" class="img-fluid">
                                </div> 
                            </div>
                        </div>
                    </div>
            </div> 
            </div>
          </div> 
        </div>




      </div>
      <!--end container-->




      <!--end container-->
    </section>
    <!--end section-->

    <!--end footer-->
    <footer class="footer footer-bar">
      <div class="container text-center">
        <div class="row align-items-center">

          <div class="col-sm-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
            <ul class="list-unstyled text-sm-end mb-0">
              <li class="list-inline-item">
                <a href="javascript:void(0)">
                  <img
                    src="images/payments/american-ex.png"
                    class="avatar avatar-ex-sm"
                    title="American Express"
                    alt=""
                  />
                </a>
              </li>
              <li class="list-inline-item">
                <a href="javascript:void(0)">
                  <img
                    src="images/payments/discover.png"
                    class="avatar avatar-ex-sm"
                    title="Discover"
                    alt=""
                  />
                </a>
              </li>
              <li class="list-inline-item">
                <a href="javascript:void(0)">
                  <img
                    src="images/payments/master-card.png"
                    class="avatar avatar-ex-sm"
                    title="Master Card"
                    alt=""
                  />
                </a>
              </li>
              <li class="list-inline-item">
                <a href="javascript:void(0)">
                  <img
                    src="images/payments/paypal.png"
                    class="avatar avatar-ex-sm"
                    title="Paypal"
                    alt=""
                  />
                </a>
              </li>
              <li class="list-inline-item">
                <a href="javascript:void(0)">
                  <img
                    src="images/payments/visa.png"
                    class="avatar avatar-ex-sm"
                    title="Visa"
                    alt=""
                  />
                </a>
              </li>
            </ul>
          </div>
          <!--end col-->
        </div>
        <!--end row-->
      </div>
      <!--end container-->
    </footer>
    <!--end footer-->
    <!-- Footer End -->

    <!-- Back to top -->
    <a
      href="#"
      onclick="topFunction()"
      id="back-to-top"
      class="btn btn-icon btn-primary back-to-top"
    >
      <i data-feather="arrow-up" class="icons"></i>
    </a>
    <!-- Back to top -->

    <!-- javascript -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <!-- SLIDER -->
    <script src="js/tiny-slider.js "></script>
    <!-- Icons -->
    <script src="js/feather.min.js"></script>
    <!-- Main Js -->
    <script src="js/plugins.init.js"></script>
    <!--Note: All init js like tiny slider, counter, countdown, maintenance, lightbox, gallery, swiper slider, aos animation etc.-->
    <script src="js/app.js"></script>
    <!--Note: All important javascript like page loader, menu, sticky menu, menu-toggler, one page menu etc. -->
  </body>
</html>
