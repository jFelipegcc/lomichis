<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?=app_name()?> | <?=$_APP['title']?></title>
    <link rel="shortcut icon" href="<?=base_url('static/images/favicon.ico')?>" type="image/x-icon">
    <link rel="icon" href="<?=base_url('static/images/favicon.ico')?>" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <link rel="stylesheet" href="<?=base_url('static/admin/font/iconsmind-s/css/iconsminds.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/font/simple-line-icons/css/simple-line-icons.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/css/vendor/bootstrap.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/css/vendor/bootstrap.rtl.only.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/css/vendor/bootstrap-float-label.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/css/main.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/css/dore.light.blue.css')?>" />    
    <link rel="stylesheet" href="<?=base_url('static/admin/fa-5.7.2/css/all.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('static/admin/toastr/toastr.min.css')?>" />
</head>
 
 
<body class="background show-spinner">
    <div class="fixed-background"></div>
<main>
    <div class="container-fluid">
        <div class='container mt-10'>
            <div class='row h-100'>
                <div class='col-12 col-md-10 mx-auto my-auto'>
                    <div class='card auth-card' >
                        <div class='position-relative image-side'  style="background-image:url('<?= base_url() ?>static/images/login-leappi.webp')" >
                            <p class=' text-white h2'>LEAPPI WEB</p>
                            <p class='white mb-0'>
                                <a href='#' class='white'>
                                    Inicia sesión
                                </a>
                                .
                            </p>
                        </div>
                        <div class='form-side' style="padding-right: 20px;padding-left: 20px;padding-top: 0;" >
                            <div class="card-header">
                                <ul class="nav nav-tabs card-header-tabs " role="tablist">
<!--                                     <li class="nav-item">
                                        <a class="nav-link active" id="first-tab" data-toggle="tab" href="#first" role="tab"
                                        aria-controls="first" aria-selected="true">Cliente</a>
                                    </li> -->
                                    <li class="nav-item">
                                        <a class="nav-link active" id="second-tab" data-toggle="tab" href="#second" role="tab"
                                        aria-controls="second" aria-selected="false">Abogado</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
<!--                                     <div class="tab-pane fade show active" id="first" role="tabpanel"
                                    aria-labelledby="first-tab">


                                    <div>
                                        <h6 class='mb-4'>Registrate como cliente</h6>
                                        <form class="needs-validation" novalidate >
                                            <label class='form-group has-float-label mb-4'>
                                                <input
                                                class='form-control rounded-right rounded-left'
                                                type='text'
                                                required
                                                minLength='3'
                                                value="" 
                                                id= "nombreCliente"

                                                />
                                                <span>Nombre</span>
                                            </label>

                                            <label class='form-group has-float-label mb-4'>
                                                <input
                                                class='form-control rounded-right rounded-left'
                                                type='text'
                                                minLength='3'
                                                required
                                                value=""
                                                id="apellidosCliente"

                                                />
                                                <span>Apellido</span>
                                            </label>

                                            <label class='form-group has-float-label mb-4'>
                                                <input
                                                class='form-control rounded-right rounded-left'
                                                type='email'
                                                required
                                                value=""
                                                id="usernameCliente"

                                                />
                                                <span>Correo eléctronico</span>
                                            </label>

                                            <label class='form-group has-float-label mb-4'>
                                                <input
                                                class='form-control rounded-right rounded-left'
                                                type='password'
                                                required
                                                value=''
                                                minLength='6'
                                                maxLength='12'
                                                placeholder=''
                                                id="passwordCliente"

                                                />
                                                <span>Contraseña</span>
                                            </label>
                                            <div class='d-flex justify-content-end align-items-center'>
                                                <button
                                                type='button'
                                                class='btn btn-primary btn-sm default mb-1'
                                                style="
                                                border-radius: 0.25rem;
                                                font-weight: bold;
                                                text-transform: capitalize;
                                                width: 50%;
                                                " 
                                                onClick='agregarCliente()'
                                                >
                                                REGISTRARSE
                                            </button>
                                        </div>
                                    </form>
                                </div>

                            </div> -->
                            <div class="tab-pane fade show active" id="second" role="tabpanel" aria-labelledby="second-tab">

                                <h6 class='mb-2'>Registrate como abogado</h6>
                                <p class='mb-4 text-danger'>Tus datos serán validos por un administrador</p>
                                <form method='post' enctype="multipart/form-data">
 
                        <!-- --------------------------- -->
                        <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Nombres <span class="text-danger">*</span></label>
                                                            <div class="form-icon position-relative">
                                                                <i data-feather="user" class="fea icon-sm icons"></i>
                                                                <input id="inputRegisterName" type="text" class="form-control ps-5" placeholder="Nombres" name="s" required="">
                                                            </div>
                                                        </div>
                                                    </div><!--end col-->

                                                    <div class="col-md-12">
                                                        <div class="mb-3"> 
                                                            <label class="form-label">Apellidos <span class="text-danger">*</span></label>
                                                            <div class="form-icon position-relative">
                                                                <i data-feather="user-check" class="fea icon-sm icons"></i>
                                                                <input id="inputRegisterAp" type="text" class="form-control ps-5" placeholder="Apellidos" name="s" required="">
                                                            </div>
                                                        </div>
                                                    </div><!--end col-->
                                                    
                                                    <div class="col-md-12">
                                                        <div class="mb-3"> 
                                                            <label class="form-label">Género <span class="text-danger">*</span></label>
                                                            <div class="form-icon position-relative">
                                                                <i data-feather="users" class="fea icon-sm icons"></i>
                                                                
                                                                <select id="selectRegisterGener" class="form-control ps-5" name="selectgenero" id="selectgenero">
                                                                    <option value="hombre">Hombre</option>
                                                                    <option value="mujer">Mujer</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div><!--end col-->
                                                    <div class="col-md-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Correo electronico <span class="text-danger">*</span></label>
                                                            <div class="form-icon position-relative">
                                                                <i data-feather="mail" class="fea icon-sm icons"></i>
                                                                <input id="inputRegisterEmail" type="email" class="form-control ps-5" placeholder="Correo electronico" name="email" required="">
                                                            </div>
                                                        </div>
                                                    </div><!--end col-->

                                                    <div class="col-md-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Contraseña <span class="text-danger">*</span></label>
                                                            <div class="form-icon position-relative">
                                                                <i data-feather="key" class="fea icon-sm icons"></i>
                                                                <input id="inputRegisterPassword" type="password" class="form-control ps-5" placeholder="Contraseña" required="">
                                                            </div>
                                                        </div>
                                                    </div><!--end col-->

                                                    <div class="col-md-12">
                                                        <div class="mb-3">
                                                            <div class="form-check">
                                                                <input id="inputRegisterChTerms" class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                                <label class="form-check-label" for="flexCheckDefault">Acepto <a href="#" class="text-primary">Terminos y codiciones</a></label>
                                                            </div>
                                                        </div>
                                                    </div><!--end col-->    
                                                    <div class="col-md-12">
                                                        <div class="d-grid">
                                                            <button type="button" onclick="Registrarse()" class="btn btn-primary default">Registrarse</button>
                                                        </div>
                                                    </div><!--end col-->

                                                    

                                                    <div class="mx-auto">
                                                        <p class="mb-0 mt-3"><small class="text-dark me-2">Ya tienes una cuenta?</small> <a href="<?php echo base_url('login'); ?>" class="text-dark fw-bold">iniciar sesión</a></p>
                                                    </div>

                        </div>                        

                    </form>
                </div>

            </div>
        </div>

    </div>
</div>
</div>
<div class='d-flex justify-content-end p-2 col-12 col-md-10 mx-auto my-auto' style="background-color: #33333">
    <a href="<?php echo base_url(); ?>">
        <p class='footer-text mt-3'>
            <i class='iconsminds-arrow-left d-block'>Regresar </i>
        </p>
    </a>
</div>
</div>
</div>
</div>
</main>

    <script type="text/javascript"> function base_url() { return "<?=base_url()?>" } </script>
    <script src="<?=base_url('static/admin/js/vendor/jquery-3.3.1.min.js')?>"></script>
    <script src="<?=base_url('static/admin/js/vendor/bootstrap.bundle.min.js')?>"></script>    
    <script src="<?=base_url('static/admin/toastr/toastr.min.js')?>"></script>
    <script src="<?=base_url('static/admin/js/dore.script.js')?>"></script>
    <script src="<?=base_url('static/admin/js/scripts.single.theme.js')?>"></script>        
 
        <script src="<?= base_url() ?>static/js/app/registro.js"></script>
    </body>
</html>